import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool hasNotify;
  final Widget? leading;
  final List<Widget> actions;
  final double? size ;

  DefaultAppBar({
    required this.title,
    this.actions = const [],
    this.leading,
    this.size,
    required this.hasNotify,

  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: MyText(
        title: "$title",
        size: 12,
        color: MyColors.white,
        fontWeight: FontWeight.bold,
      ),
      centerTitle: true,
      backgroundColor: MyColors.primary,
      elevation: 0,
      leading: leading ??
          IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 25,
              color: MyColors.white,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
      actions: hasNotify == true ?  [
       InkWell(
          onTap: ()=> AutoRouter.of(context).push(AlertsRoute()),
            child: Image.asset(Res.notifications, scale: 3,))
      ]:[]
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(size??65);
}
