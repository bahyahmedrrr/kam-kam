import 'dart:ui';
import 'package:flutter/material.dart';

class MyColors{
  static Color primary =  Color(0xff3E8B3A);
  static Color secondary =  Color(0xff2A4571);
  static Color headerColor =  Color(0xff6e7c87);
  static Color bg=Color(0xffF3F6FA);
  static Color lang=Color(0xff95BAE2);
  static Color red=Color(0xffB82925);
  static Color offWhite=Color(0xffFFFFFF);
  static Color card1bg=Color(0xffD1E8CE);
  static Color card2bg=Color(0xffF3CBD2);
  static Color gold=Color(0xffe4aa69);
  static Color grey=Colors.grey;
  static Color greyWhite=Colors.grey.withOpacity(.2);
  static Color black=Color(0xff031626);
  static Color blackOpacity=Colors.black54;
  static Color white=Colors.white;
  static Color notifyColor=Colors.black54;



}