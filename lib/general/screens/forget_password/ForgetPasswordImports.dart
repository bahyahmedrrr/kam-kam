import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:base_flutter/general/resources/GeneralRepoImports.dart';
import 'package:base_flutter/general/widgets/AuthScaffold.dart';
import 'widgets/ForgetPasswordWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:base_flutter/general/widgets/HeaderLogo.dart';

part 'ForgetPasswordData.dart';
part 'ForgetPassword.dart';
