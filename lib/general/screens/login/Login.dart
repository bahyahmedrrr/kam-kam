part of 'LoginImports.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}
class _LoginState extends State<Login> {
  LoginData loginData = new LoginData();
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: AuthScaffold(
        child: Column(
          children: [
            BuildText(),
            BuildTabBar(),
            BuildTabBarView(),
            BuildForgetText(),
            BuildLoginButton(loginData: loginData),
            BuildNewRegister(),
          ],
        ),
      ),
    );
  }
}
