import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/login/LoginImports.dart';
import 'package:base_flutter/general/screens/login/tabs/e_mail/widgets/EmailWidgetsImports.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/tf_validator.dart';

part 'EmailData.dart';
part 'Email.dart';
