import 'package:base_flutter/customer/screens/register/widgets/RegisterWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/login/tabs/e_mail/EmailImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildEmailInputs.dart';