part of 'EmailWidgetsImports.dart';

class BuildEmailInputs extends StatelessWidget {
  final EmailData emailData;
  const BuildEmailInputs({required this.emailData});
  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InputName(text: "البريد الالكتروني"),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "mail"),
            controller: emailData.email,
            fillColor: MyColors.offWhite,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.emailAddress,
            validate: (value) => value!.validateEmpty(context),
          ),
          InputName(text: "كلمة المرور"),
          GenericTextField(
            fieldTypes: FieldTypes.password,
            label: tr(context, "password"),
            controller: emailData.password,
            fillColor: MyColors.offWhite,
            validate: (value) => value!.validateEmpty(context),
            type: TextInputType.text,
            action: TextInputAction.done,
            onSubmit: () => emailData.userLogin(context),
          ),
        ],
      ),
    );
  }
}
