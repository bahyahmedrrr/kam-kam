part of 'EmailImports.dart';

class Email extends StatefulWidget {
  const Email({Key? key}) : super(key: key);

  @override
  _EmailState createState() => _EmailState();
}

class _EmailState extends State<Email> {
  @override
  Widget build(BuildContext context) {
    final EmailData emailData = EmailData();
    return Scaffold(
      backgroundColor: MyColors.bg,
      body: BuildEmailInputs(emailData: emailData),
    );
  }
}
