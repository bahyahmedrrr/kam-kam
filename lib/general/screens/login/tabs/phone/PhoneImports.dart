import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/login/tabs/phone/widgets/PhoneWidgetsImports.dart';
import 'package:flutter/material.dart';

import '../../../../utilities/routers/RouterImports.gr.dart';

part 'PhoneData.dart';
part 'Phone.dart';
