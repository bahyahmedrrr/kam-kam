part of 'PhoneImports.dart';
class Phone extends StatefulWidget {
  const Phone({Key? key}) : super(key: key);

  @override
  _PhoeState createState() => _PhoeState();
}

class _PhoeState extends State<Phone> {
  final PhoneData phoneData = PhoneData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      body: BuildPhoneInputs(phoneData: phoneData,) ,
    );
  }
}
