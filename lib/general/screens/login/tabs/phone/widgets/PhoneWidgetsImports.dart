import 'package:base_flutter/customer/screens/register/widgets/RegisterWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/login/tabs/phone/PhoneImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';

part 'BuildPhoneInputs.dart';