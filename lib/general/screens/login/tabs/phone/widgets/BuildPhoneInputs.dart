part of 'PhoneWidgetsImports.dart';
class BuildPhoneInputs extends StatelessWidget {
  final PhoneData phoneData ;
  const BuildPhoneInputs({required this.phoneData}) ;

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InputName(text: "الهاتف"),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "mail"),
            controller: phoneData.phone,
            fillColor: MyColors.offWhite,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.emailAddress,
            validate: (value) => value!.validateEmpty(context),
          ),
          InputName(text: "كلمة المرور"),
          GenericTextField(
            fieldTypes: FieldTypes.password,
            label: tr(context, "password"),
            controller: phoneData.password,
            fillColor: MyColors.offWhite,
            validate: (value) => value!.validateEmpty(context),
            type: TextInputType.text,
            action: TextInputAction.done,
            onSubmit: () => phoneData.userLogin(context),
          ),
        ],
      ),
    );
  }
}
