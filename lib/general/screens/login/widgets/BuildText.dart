part of 'LoginWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MyText(title: "اهلا بك وبعودتك ", color: MyColors.black, size: 15, fontWeight: FontWeight.bold,),
        MyText(
          title: "إختر طريقة دخولك",
          size: 11,
          color: MyColors.primary,
        ),
      ],
    );
  }
}
