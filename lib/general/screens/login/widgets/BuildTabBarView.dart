part of 'LoginWidgetsImports.dart';

class BuildTabBarView extends StatelessWidget {
  const BuildTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TabBarView(
        children: [
          Phone(),
          Email()
        ],
      ),
    );
  }
}
