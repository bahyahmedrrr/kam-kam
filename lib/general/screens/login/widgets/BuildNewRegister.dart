part of 'LoginWidgetsImports.dart';

class BuildNewRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title:tr(context,"don'tHaveAccount"),
                size: 12,
                alien: TextAlign.center,
                color: MyColors.black,
              ),
              InkWell(
                onTap: ()=> AutoRouter.of(context).push(RegisterRoute()),
                child: MyText(
                  title:"تسجيل جديد",
                  size: 12,
                  alien: TextAlign.center,
                  color: MyColors.primary,
                ),
              ),
            ],
          ),
          InkWell(
            onTap: ()=> AutoRouter.of(context).push(RegisterRoute()),
            child: MyText(
              title:"الدخول كذائر",
              size: 12,
              alien: TextAlign.center,
              color: MyColors.primary,
              decoration: TextDecoration.underline,
            ),
          ),
        ],
      ),
    );
  }
}
