part of 'LoginWidgetsImports.dart';
class BuildTabBar extends StatelessWidget {
  const BuildTabBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
      padding: const EdgeInsets.symmetric(horizontal: 20),
        indicatorSize: TabBarIndicatorSize.label,
        automaticIndicatorColorAdjustment: false,
        indicatorColor: MyColors.primary,
        unselectedLabelColor: MyColors.grey,
        labelColor: MyColors.primary,
        tabs: [
      Text("رقم الجوال", ),
      Text("البريد الاكتروني"),
    ]);
  }
}
