part of 'ActiveAccountWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MyText(
            title:"أهلا بك وبعودتك",
            size: 14,
            color: MyColors.black,
          ),
          MyText(
            title: "يرجي إدخال كود التفعيل المرسل ل لجوالك",
            size: 12,
            color: MyColors.primary.withOpacity(.8),
          ),
        ],
      ),
    );
  }
}
