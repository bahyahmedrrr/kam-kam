part of 'ActiveAccountWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  final ActiveAccountData activeAccountData;
  final String userId;

  const BuildButtonList(
      {required this.activeAccountData, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
            onTap: () => activeAccountData.onResendCode(context, userId),
            child: MyText(
              title: "اعادة ارسال الكود",
              size: 13,
              color: MyColors.primary,
            )),
        LoadingButton(
          btnKey: activeAccountData.btnKey,
          title: "تأكيد",
          onTap: () => AutoRouter.of(context).push(SubscribtionPackageRoute()),
          color: MyColors.primary,
          margin: const EdgeInsets.symmetric(
            vertical: 30,
          ),
        )
      ],
    );
  }
}
