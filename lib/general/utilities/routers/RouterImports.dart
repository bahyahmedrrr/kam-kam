import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/alerts/AlertsImports.dart';
import 'package:base_flutter/customer/screens/complaints/ComplaintsImports.dart';
import 'package:base_flutter/customer/screens/done-pay/DonePayImports.dart';
import 'package:base_flutter/customer/screens/favourite/FavouriteImports.dart';
import 'package:base_flutter/customer/screens/league-details/LeagueDetailsImports.dart';
import 'package:base_flutter/customer/screens/match-guessing/MatchGuessingImports.dart';
import 'package:base_flutter/customer/screens/match-result/MatchResultImports.dart';
import 'package:base_flutter/customer/screens/profile-data/ProfileImports.dart';
import 'package:base_flutter/customer/screens/questions/QuestionsImports.dart';
import 'package:base_flutter/customer/screens/select-package/SelectPackageImports.dart';
import 'package:base_flutter/customer/screens/team-matches/TeamMatchesImports.dart';
import 'package:base_flutter/general/screens/about/AboutImports.dart';
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart';
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart';
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart';
import 'package:base_flutter/customer//screens/contact-us/ContactUsImports.dart';
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart';
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart';
import 'package:base_flutter/general/screens/login/LoginImports.dart';
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart';
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart';
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart';
import 'package:base_flutter/general/screens/splash/SplashImports.dart';
import 'package:base_flutter/general/screens/terms/TermsImports.dart';

import '../../../customer/screens/SwitchActiveMethod/SwitchActiveMethodImports.dart';
import '../../../customer/screens/biography/BiographyImports.dart';
import '../../../customer/screens/home/HomeImports.dart';
import '../../../customer/screens/langauage/LangaugeImports.dart';
import '../../../customer/screens/register/RegisterImports.dart';
import '../../../customer/screens/subscribtion_package/SubscribtionPackageImports.dart';


part 'Router.dart';