// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i31;
import 'package:base_flutter/customer//screens/contact-us/ContactUsImports.dart'
    as _i23;
import 'package:base_flutter/customer/screens/alerts/AlertsImports.dart'
    as _i30;
import 'package:base_flutter/customer/screens/biography/BiographyImports.dart'
    as _i13;
import 'package:base_flutter/customer/screens/complaints/ComplaintsImports.dart'
    as _i24;
import 'package:base_flutter/customer/screens/done-pay/DonePayImports.dart'
    as _i18;
import 'package:base_flutter/customer/screens/favourite/FavouriteImports.dart'
    as _i22;
import 'package:base_flutter/customer/screens/home/HomeImports.dart' as _i19;
import 'package:base_flutter/customer/screens/langauage/LangaugeImports.dart'
    as _i20;
import 'package:base_flutter/customer/screens/league-details/LeagueDetailsImports.dart'
    as _i25;
import 'package:base_flutter/customer/screens/match-guessing/MatchGuessingImports.dart'
    as _i27;
import 'package:base_flutter/customer/screens/match-result/MatchResultImports.dart'
    as _i26;
import 'package:base_flutter/customer/screens/profile-data/ProfileImports.dart'
    as _i28;
import 'package:base_flutter/customer/screens/questions/QuestionsImports.dart'
    as _i21;
import 'package:base_flutter/customer/screens/register/RegisterImports.dart'
    as _i14;
import 'package:base_flutter/customer/screens/select-package/SelectPackageImports.dart'
    as _i17;
import 'package:base_flutter/customer/screens/subscribtion_package/SubscribtionPackageImports.dart'
    as _i16;
import 'package:base_flutter/customer/screens/SwitchActiveMethod/SwitchActiveMethodImports.dart'
    as _i15;
import 'package:base_flutter/customer/screens/team-matches/TeamMatchesImports.dart'
    as _i29;
import 'package:base_flutter/general/screens/about/AboutImports.dart' as _i8;
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart'
    as _i4;
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart'
    as _i11;
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart'
    as _i10;
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart'
    as _i3;
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart' as _i12;
import 'package:base_flutter/general/screens/login/LoginImports.dart' as _i2;
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart'
    as _i5;
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart'
    as _i6;
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart'
    as _i9;
import 'package:base_flutter/general/screens/splash/SplashImports.dart' as _i1;
import 'package:base_flutter/general/screens/terms/TermsImports.dart' as _i7;
import 'package:flutter/material.dart' as _i32;

class AppRouter extends _i31.RootStackRouter {
  AppRouter([_i32.GlobalKey<_i32.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i31.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      final args = routeData.argsAs<SplashRouteArgs>();
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i1.Splash(navigatorKey: args.navigatorKey));
    },
    LoginRoute.name: (routeData) {
      return _i31.CustomPage<dynamic>(
          routeData: routeData,
          child: _i2.Login(),
          opaque: true,
          barrierDismissible: false);
    },
    ForgetPasswordRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i3.ForgetPassword());
    },
    ActiveAccountRoute.name: (routeData) {
      final args = routeData.argsAs<ActiveAccountRouteArgs>();
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i4.ActiveAccount(userId: args.userId));
    },
    ResetPasswordRoute.name: (routeData) {
      final args = routeData.argsAs<ResetPasswordRouteArgs>();
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i5.ResetPassword(userId: args.userId));
    },
    SelectLangRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i6.SelectLang());
    },
    TermsRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i7.Terms());
    },
    AboutRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i8.About());
    },
    SelectUserRoute.name: (routeData) {
      return _i31.CustomPage<dynamic>(
          routeData: routeData,
          child: _i9.SelectUser(),
          transitionsBuilder: _i31.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 1500,
          opaque: true,
          barrierDismissible: false);
    },
    ConfirmPasswordRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i10.ConfirmPassword());
    },
    ChangePasswordRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i11.ChangePassword());
    },
    ImageZoomRoute.name: (routeData) {
      final args = routeData.argsAs<ImageZoomRouteArgs>();
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: _i12.ImageZoom(images: args.images));
    },
    BiographyRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i13.Biography());
    },
    RegisterRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i14.Register());
    },
    SwitchActiveMethodRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i15.SwitchActiveMethod());
    },
    SubscribtionPackageRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i16.SubscribtionPackage());
    },
    SelectPackageRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i17.SelectPackage());
    },
    DonePayRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i18.DonePay());
    },
    HomeRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i19.Home());
    },
    LangaugeRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i20.Langauge());
    },
    QuestionsRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i21.Questions());
    },
    FavouriteRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i22.Favourite());
    },
    ContactUsRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i23.ContactUs());
    },
    ComplaintsRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i24.Complaints());
    },
    LeagueDetailsRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i25.LeagueDetails());
    },
    MatchResultRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i26.MatchResult());
    },
    MatchGuessingRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i27.MatchGuessing());
    },
    ProfileRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i28.Profile());
    },
    TeamMatchesRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i29.TeamMatches());
    },
    AlertsRoute.name: (routeData) {
      return _i31.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i30.Alerts());
    }
  };

  @override
  List<_i31.RouteConfig> get routes => [
        _i31.RouteConfig(SplashRoute.name, path: '/'),
        _i31.RouteConfig(LoginRoute.name, path: '/Login'),
        _i31.RouteConfig(ForgetPasswordRoute.name, path: '/forget-password'),
        _i31.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i31.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i31.RouteConfig(SelectLangRoute.name, path: '/select-lang'),
        _i31.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i31.RouteConfig(AboutRoute.name, path: '/About'),
        _i31.RouteConfig(SelectUserRoute.name, path: '/select-user'),
        _i31.RouteConfig(ConfirmPasswordRoute.name, path: '/confirm-password'),
        _i31.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i31.RouteConfig(ImageZoomRoute.name, path: '/image-zoom'),
        _i31.RouteConfig(BiographyRoute.name, path: '/Biography'),
        _i31.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i31.RouteConfig(SwitchActiveMethodRoute.name,
            path: '/switch-active-method'),
        _i31.RouteConfig(SubscribtionPackageRoute.name,
            path: '/subscribtion-package'),
        _i31.RouteConfig(SelectPackageRoute.name, path: '/select-package'),
        _i31.RouteConfig(DonePayRoute.name, path: '/done-pay'),
        _i31.RouteConfig(HomeRoute.name, path: '/Home'),
        _i31.RouteConfig(LangaugeRoute.name, path: '/Langauge'),
        _i31.RouteConfig(QuestionsRoute.name, path: '/Questions'),
        _i31.RouteConfig(FavouriteRoute.name, path: '/Favourite'),
        _i31.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i31.RouteConfig(ComplaintsRoute.name, path: '/Complaints'),
        _i31.RouteConfig(LeagueDetailsRoute.name, path: '/league-details'),
        _i31.RouteConfig(MatchResultRoute.name, path: '/match-result'),
        _i31.RouteConfig(MatchGuessingRoute.name, path: '/match-guessing'),
        _i31.RouteConfig(ProfileRoute.name, path: '/Profile'),
        _i31.RouteConfig(TeamMatchesRoute.name, path: '/team-matches'),
        _i31.RouteConfig(AlertsRoute.name, path: '/Alerts')
      ];
}

/// generated route for
/// [_i1.Splash]
class SplashRoute extends _i31.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({required _i32.GlobalKey<_i32.NavigatorState> navigatorKey})
      : super(SplashRoute.name,
            path: '/', args: SplashRouteArgs(navigatorKey: navigatorKey));

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({required this.navigatorKey});

  final _i32.GlobalKey<_i32.NavigatorState> navigatorKey;

  @override
  String toString() {
    return 'SplashRouteArgs{navigatorKey: $navigatorKey}';
  }
}

/// generated route for
/// [_i2.Login]
class LoginRoute extends _i31.PageRouteInfo<void> {
  const LoginRoute() : super(LoginRoute.name, path: '/Login');

  static const String name = 'LoginRoute';
}

/// generated route for
/// [_i3.ForgetPassword]
class ForgetPasswordRoute extends _i31.PageRouteInfo<void> {
  const ForgetPasswordRoute()
      : super(ForgetPasswordRoute.name, path: '/forget-password');

  static const String name = 'ForgetPasswordRoute';
}

/// generated route for
/// [_i4.ActiveAccount]
class ActiveAccountRoute extends _i31.PageRouteInfo<ActiveAccountRouteArgs> {
  ActiveAccountRoute({required String userId})
      : super(ActiveAccountRoute.name,
            path: '/active-account',
            args: ActiveAccountRouteArgs(userId: userId));

  static const String name = 'ActiveAccountRoute';
}

class ActiveAccountRouteArgs {
  const ActiveAccountRouteArgs({required this.userId});

  final String userId;

  @override
  String toString() {
    return 'ActiveAccountRouteArgs{userId: $userId}';
  }
}

/// generated route for
/// [_i5.ResetPassword]
class ResetPasswordRoute extends _i31.PageRouteInfo<ResetPasswordRouteArgs> {
  ResetPasswordRoute({required String userId})
      : super(ResetPasswordRoute.name,
            path: '/reset-password',
            args: ResetPasswordRouteArgs(userId: userId));

  static const String name = 'ResetPasswordRoute';
}

class ResetPasswordRouteArgs {
  const ResetPasswordRouteArgs({required this.userId});

  final String userId;

  @override
  String toString() {
    return 'ResetPasswordRouteArgs{userId: $userId}';
  }
}

/// generated route for
/// [_i6.SelectLang]
class SelectLangRoute extends _i31.PageRouteInfo<void> {
  const SelectLangRoute() : super(SelectLangRoute.name, path: '/select-lang');

  static const String name = 'SelectLangRoute';
}

/// generated route for
/// [_i7.Terms]
class TermsRoute extends _i31.PageRouteInfo<void> {
  const TermsRoute() : super(TermsRoute.name, path: '/Terms');

  static const String name = 'TermsRoute';
}

/// generated route for
/// [_i8.About]
class AboutRoute extends _i31.PageRouteInfo<void> {
  const AboutRoute() : super(AboutRoute.name, path: '/About');

  static const String name = 'AboutRoute';
}

/// generated route for
/// [_i9.SelectUser]
class SelectUserRoute extends _i31.PageRouteInfo<void> {
  const SelectUserRoute() : super(SelectUserRoute.name, path: '/select-user');

  static const String name = 'SelectUserRoute';
}

/// generated route for
/// [_i10.ConfirmPassword]
class ConfirmPasswordRoute extends _i31.PageRouteInfo<void> {
  const ConfirmPasswordRoute()
      : super(ConfirmPasswordRoute.name, path: '/confirm-password');

  static const String name = 'ConfirmPasswordRoute';
}

/// generated route for
/// [_i11.ChangePassword]
class ChangePasswordRoute extends _i31.PageRouteInfo<void> {
  const ChangePasswordRoute()
      : super(ChangePasswordRoute.name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

/// generated route for
/// [_i12.ImageZoom]
class ImageZoomRoute extends _i31.PageRouteInfo<ImageZoomRouteArgs> {
  ImageZoomRoute({required List<dynamic> images})
      : super(ImageZoomRoute.name,
            path: '/image-zoom', args: ImageZoomRouteArgs(images: images));

  static const String name = 'ImageZoomRoute';
}

class ImageZoomRouteArgs {
  const ImageZoomRouteArgs({required this.images});

  final List<dynamic> images;

  @override
  String toString() {
    return 'ImageZoomRouteArgs{images: $images}';
  }
}

/// generated route for
/// [_i13.Biography]
class BiographyRoute extends _i31.PageRouteInfo<void> {
  const BiographyRoute() : super(BiographyRoute.name, path: '/Biography');

  static const String name = 'BiographyRoute';
}

/// generated route for
/// [_i14.Register]
class RegisterRoute extends _i31.PageRouteInfo<void> {
  const RegisterRoute() : super(RegisterRoute.name, path: '/Register');

  static const String name = 'RegisterRoute';
}

/// generated route for
/// [_i15.SwitchActiveMethod]
class SwitchActiveMethodRoute extends _i31.PageRouteInfo<void> {
  const SwitchActiveMethodRoute()
      : super(SwitchActiveMethodRoute.name, path: '/switch-active-method');

  static const String name = 'SwitchActiveMethodRoute';
}

/// generated route for
/// [_i16.SubscribtionPackage]
class SubscribtionPackageRoute extends _i31.PageRouteInfo<void> {
  const SubscribtionPackageRoute()
      : super(SubscribtionPackageRoute.name, path: '/subscribtion-package');

  static const String name = 'SubscribtionPackageRoute';
}

/// generated route for
/// [_i17.SelectPackage]
class SelectPackageRoute extends _i31.PageRouteInfo<void> {
  const SelectPackageRoute()
      : super(SelectPackageRoute.name, path: '/select-package');

  static const String name = 'SelectPackageRoute';
}

/// generated route for
/// [_i18.DonePay]
class DonePayRoute extends _i31.PageRouteInfo<void> {
  const DonePayRoute() : super(DonePayRoute.name, path: '/done-pay');

  static const String name = 'DonePayRoute';
}

/// generated route for
/// [_i19.Home]
class HomeRoute extends _i31.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: '/Home');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i20.Langauge]
class LangaugeRoute extends _i31.PageRouteInfo<void> {
  const LangaugeRoute() : super(LangaugeRoute.name, path: '/Langauge');

  static const String name = 'LangaugeRoute';
}

/// generated route for
/// [_i21.Questions]
class QuestionsRoute extends _i31.PageRouteInfo<void> {
  const QuestionsRoute() : super(QuestionsRoute.name, path: '/Questions');

  static const String name = 'QuestionsRoute';
}

/// generated route for
/// [_i22.Favourite]
class FavouriteRoute extends _i31.PageRouteInfo<void> {
  const FavouriteRoute() : super(FavouriteRoute.name, path: '/Favourite');

  static const String name = 'FavouriteRoute';
}

/// generated route for
/// [_i23.ContactUs]
class ContactUsRoute extends _i31.PageRouteInfo<void> {
  const ContactUsRoute() : super(ContactUsRoute.name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

/// generated route for
/// [_i24.Complaints]
class ComplaintsRoute extends _i31.PageRouteInfo<void> {
  const ComplaintsRoute() : super(ComplaintsRoute.name, path: '/Complaints');

  static const String name = 'ComplaintsRoute';
}

/// generated route for
/// [_i25.LeagueDetails]
class LeagueDetailsRoute extends _i31.PageRouteInfo<void> {
  const LeagueDetailsRoute()
      : super(LeagueDetailsRoute.name, path: '/league-details');

  static const String name = 'LeagueDetailsRoute';
}

/// generated route for
/// [_i26.MatchResult]
class MatchResultRoute extends _i31.PageRouteInfo<void> {
  const MatchResultRoute()
      : super(MatchResultRoute.name, path: '/match-result');

  static const String name = 'MatchResultRoute';
}

/// generated route for
/// [_i27.MatchGuessing]
class MatchGuessingRoute extends _i31.PageRouteInfo<void> {
  const MatchGuessingRoute()
      : super(MatchGuessingRoute.name, path: '/match-guessing');

  static const String name = 'MatchGuessingRoute';
}

/// generated route for
/// [_i28.Profile]
class ProfileRoute extends _i31.PageRouteInfo<void> {
  const ProfileRoute() : super(ProfileRoute.name, path: '/Profile');

  static const String name = 'ProfileRoute';
}

/// generated route for
/// [_i29.TeamMatches]
class TeamMatchesRoute extends _i31.PageRouteInfo<void> {
  const TeamMatchesRoute()
      : super(TeamMatchesRoute.name, path: '/team-matches');

  static const String name = 'TeamMatchesRoute';
}

/// generated route for
/// [_i30.Alerts]
class AlertsRoute extends _i31.PageRouteInfo<void> {
  const AlertsRoute() : super(AlertsRoute.name, path: '/Alerts');

  static const String name = 'AlertsRoute';
}
