part of 'RouterImports.dart';


@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //general routes
    AdaptiveRoute(page: Splash, initial: true,),
    CustomRoute(page: Login,),
    AdaptiveRoute(page: ForgetPassword),
    AdaptiveRoute(page: ActiveAccount),
    AdaptiveRoute(page: ResetPassword),
    AdaptiveRoute(page: SelectLang),
    AdaptiveRoute(page: Terms),
    AdaptiveRoute(page: About),
    CustomRoute(page: SelectUser,transitionsBuilder: TransitionsBuilders.fadeIn,durationInMilliseconds: 1500),
    AdaptiveRoute(page: ConfirmPassword),
    AdaptiveRoute(page: ChangePassword),
    AdaptiveRoute(page: ImageZoom),
    // customer routes
    AdaptiveRoute(page: Biography),
    AdaptiveRoute(page: Register),
    AdaptiveRoute(page: SwitchActiveMethod),
    AdaptiveRoute(page: SubscribtionPackage),
    AdaptiveRoute(page: SelectPackage),
    AdaptiveRoute(page: DonePay),
    AdaptiveRoute(page: Home),
    AdaptiveRoute(page: Langauge),
    AdaptiveRoute(page: Questions),
    AdaptiveRoute(page: Favourite),
    AdaptiveRoute(page: ContactUs),
    AdaptiveRoute(page: Complaints),
    AdaptiveRoute(page: LeagueDetails),
    AdaptiveRoute(page: MatchResult),
    AdaptiveRoute(page: MatchGuessing),
    AdaptiveRoute(page: Profile),
    AdaptiveRoute(page: TeamMatches),
    AdaptiveRoute(page: Alerts),
  ],
)
class $AppRouter {}