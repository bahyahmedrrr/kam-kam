part of 'ComplaintsImports.dart';

class Complaints extends StatefulWidget {
  const Complaints({Key? key}) : super(key: key);

  @override
  _ComplaintsState createState() => _ComplaintsState();
}

class _ComplaintsState extends State<Complaints> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.bg,
        appBar: DefaultAppBar(
          title: "الشكاوي و الإقتراحات",
          hasNotify: false,
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          children: [
            BuildComplaintsInputs(),
          ],
        ),
        bottomSheet: DefaultButton(title: "إرسال", onTap: () {}));
  }
}
