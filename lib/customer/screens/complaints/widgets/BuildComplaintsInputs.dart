part of 'ComplaintsWidgetsImports.dart';

class BuildComplaintsInputs extends StatelessWidget {
  const BuildComplaintsInputs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InputName(text: "نوع الرساله"),
        GenericTextField(
          fillColor: MyColors.offWhite,
          fieldTypes: FieldTypes.normal,
          hint: "نص افتراضي",
          margin: const EdgeInsets.symmetric(vertical: 3),
          action: TextInputAction.next,
          type: TextInputType.name,
          validate: (value) => value!.validateEmpty(context),
          contentPadding: const EdgeInsets.symmetric(vertical: 5,horizontal: 8),
        ),
        InputName(text: "عنوان الرساله"),
        GenericTextField(
          fillColor: MyColors.offWhite,
          fieldTypes: FieldTypes.normal,
          hint: "نص افتراضي",
          margin: const EdgeInsets.symmetric(vertical: 3),
          action: TextInputAction.next,
          type: TextInputType.name,
          validate: (value) => value!.validateEmpty(context),
          contentPadding: const EdgeInsets.symmetric(vertical: 5,horizontal: 8),
        ),
        InputName(text: "محتوي الرساله"),
        GenericTextField(
          fillColor: MyColors.offWhite,
          fieldTypes: FieldTypes.rich,
          max: 6,
          hint: "نص افتراضي",
          margin: const EdgeInsets.symmetric(vertical: 3),
          action: TextInputAction.next,
          type: TextInputType.name,
          validate: (value) => value!.validateEmpty(context),
          contentPadding: const EdgeInsets.symmetric(vertical: 5,horizontal: 8),
        ),
        InputName(text: "إرفاق صوره"),
        Container(
          color: MyColors.offWhite,
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
          child: Image.asset(Res.upload, scale: 3,),
        )
      ],
    );
  }
}
