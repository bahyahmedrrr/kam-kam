part of 'ComplaintsWidgetsImports.dart';

class InputName extends StatelessWidget {
  final String text;
  const InputName({required this.text});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: MyText(title: text, color: MyColors.black, size: 10, fontWeight: FontWeight.bold,),
    );
  }
}
