part of 'FavouriteWidgetsImports.dart';

class BuildFavCard extends StatelessWidget {
  const BuildFavCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(color: MyColors.offWhite),
      child: Row(
        children: [
          CachedImage(
              borderColor: MyColors.grey.withOpacity(.2),
              borderRadius: BorderRadius.circular(15),
              height: 25,
              width: 25,
              fit: BoxFit.fill,
              url:
                  "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
          SizedBox(
            width: 10,
          ),
          MyText(
            title: "الشاب السعودي",
            color: MyColors.black,
            size: 12,
            fontWeight: FontWeight.bold,
          ),
          Spacer(),
          InkWell(
            child: Image.asset(
              Res.heartactive,
              scale: 4,
            ),
          )
        ],
      ),
    );
  }
}
