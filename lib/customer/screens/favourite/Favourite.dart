part of 'FavouriteImports.dart';
class Favourite extends StatefulWidget {
  const Favourite({Key? key}) : super(key: key);

  @override
  _FavouriteState createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: "المفضله", hasNotify: false,),
      body: ListView.builder(
        itemCount: 3,
        itemBuilder: (context, index) => BuildFavCard(),
      ),
    );
  }
}
