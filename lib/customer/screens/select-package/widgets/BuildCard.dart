part of 'SelectPackageWidgetsImports.dart';
class BuildCard  extends StatelessWidget {
  final String? type;
  final String img ;
  const BuildCard ({ this.type,required this.img});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> AutoRouter.of(context).push(DonePayRoute()),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height*.12,
        margin: const EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: MyColors.offWhite,
          borderRadius: BorderRadius.circular(25)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(img, scale: 3,),
            MyText(title: type??'', color: MyColors.black, size: 12,)
          ],
        ),
      ),
    );
  }
}
