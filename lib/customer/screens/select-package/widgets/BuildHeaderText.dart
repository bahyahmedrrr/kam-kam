part of 'SelectPackageWidgetsImports.dart';
class BuildHeaderText extends StatelessWidget {
  const BuildHeaderText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MyText(title: "يرجي دفع 130 ريال تكلفة الباقه", color: MyColors.black, size: 14),
          MyText(title: "السعر شامل القيمه المضافه", color: MyColors.red, size: 14),
        ],
      ),
    );
  }
}
