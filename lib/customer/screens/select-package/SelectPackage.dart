part of 'SelectPackageImports.dart';
class SelectPackage extends StatefulWidget {
  const SelectPackage({Key? key}) : super(key: key);

  @override
  _SelectPackageState createState() => _SelectPackageState();
}

class _SelectPackageState extends State<SelectPackage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "باقات الاشتراك", hasNotify: false,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        children: [
          BuildHeaderText(),
          BuildCard(img: Res.credit, type: "البطاقه الائتمانيه",),
          BuildCard(img: Res.stcpay, ),
          BuildCard(img: Res.applepay, ),
        ],
      ),
    );
  }
}
