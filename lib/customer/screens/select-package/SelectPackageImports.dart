import 'package:base_flutter/customer/screens/select-package/widgets/SelectPackageWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'SelectPackage.dart';
part 'SelectPackageData.dart';
