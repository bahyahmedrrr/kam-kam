part of 'TeamMatchesImports.dart';
class TeamMatches extends StatefulWidget {
  const TeamMatches({Key? key}) : super(key: key);

  @override
  _TeamMatchesState createState() => _TeamMatchesState();
}

class _TeamMatchesState extends State<TeamMatches> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "الهلال السعودي", hasNotify: false,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        children: List.generate(3, (index) => BuildMatchDetailsCard() )
      ),
    );
  }
}
