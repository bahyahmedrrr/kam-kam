part of 'TeamMatchesWidgetsImports.dart';

class BuildMatchDetailsCard extends StatelessWidget {
  const BuildMatchDetailsCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      color: MyColors.offWhite,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                  title: "الدوري السعودي للمحترفين",
                  color: MyColors.black,
                  size: 12),
              MyText(
                  title: "الجمعه 21 يناير", color: MyColors.primary, size: 10),
            ],
          ),
          Divider(),
          BuildResultCard(
              result: "4",
              result2: "2",
              name: "الاهلي ",
              name1: "الزمالك",
              isEnd: true)
        ],
      ),
    );
  }
}
