import 'package:base_flutter/customer/screens/team-matches/widgets/TeamMatchesWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';

part 'TeamMatchesData.dart';
part 'TeamMatches.dart';
