part of 'BiographyImports.dart';

class Biography extends StatefulWidget {
  const Biography({Key? key}) : super(key: key);

  @override
  _BiographyState createState() => _BiographyState();
}

class _BiographyState extends State<Biography> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.offWhite,
      body: ListView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(
          vertical: 30,
        ),
        children: [
          BuildImage(),
          BuildTextCard(),
        ],
      ),
      bottomSheet: DefaultButton(title: "دخول", onTap: () => AutoRouter.of(context).push(LoginRoute())),
    );
  }
}
