part of 'BiographyWidgetsImports.dart';

class BuildTextCard extends StatelessWidget {
  const BuildTextCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          color: MyColors.bg,
          borderRadius: BorderRadiusDirectional.only(
              topStart: Radius.circular(50), topEnd: Radius.circular(50))),
      child: Column(
        children: [
          MyText(
              title: "نبذه تعريفيه عن كم كم ",
              color: MyColors.primary,
              size: 15),
          MyText(
              title:
                  "api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api api ",
              color: MyColors.black,
              size: 14),
        ],
      ),
    );
  }
}
