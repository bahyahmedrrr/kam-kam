part of 'SubscribtionPackageWidgetsImports.dart';

class BuildSelectPackage extends StatelessWidget {
  final SubscribtionPackageData data;

  const BuildSelectPackage({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: MyText(
            title: "إختر الباقه المناسبه لك",
            color: MyColors.black,
            size: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BuildPackageItem(
              type: "مجانيه",
              price: "0",
              bg: MyColors.offWhite,
              select: 1,
              data: data,
            ),
            BuildPackageItem(
              type: "مجانيه",
              price: "100",
              bg: MyColors.offWhite,
              select: 2,
              data: data,
            ),
            BuildPackageItem(
              type: "مجانيه",
              price: "0",
              bg: MyColors.offWhite,
              select: 3,
              data: data,
            ),
          ],
        )
      ],
    );
  }
}
