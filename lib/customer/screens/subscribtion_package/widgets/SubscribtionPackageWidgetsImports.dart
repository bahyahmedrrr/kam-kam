import 'package:base_flutter/customer/screens/subscribtion_package/SubscribtionPackageImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildPackageItem.dart';
part 'BuildSelectPackage.dart';
part 'BuildFeaturesOfPackage.dart';