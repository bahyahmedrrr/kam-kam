part of 'SubscribtionPackageWidgetsImports.dart';

class BuildPackageItem extends StatelessWidget {
  final SubscribtionPackageData data;

  final int select;
  final Color? color;
  final Color? color2;
  final Color bg;
  final String type;
  final String price;

  const BuildPackageItem(
      {this.color,
      required this.type,
      required this.price,
      this.color2,
      required this.bg,
      required this.select,
      required this.data});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
        bloc: data.selection,
        builder: (context, state) {
          return InkWell(
            onTap: ()=> data.selection.onUpdateData(select),
            child: Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width * .25,
              decoration: BoxDecoration(
                  color: state.data == select ?  MyColors.primary: bg,
                  border: Border.all(color: MyColors.grey.withOpacity(0.2)),
                borderRadius: BorderRadius.circular(15)
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                children: [
                  MyText(
                      title: type, color: color2 ?? MyColors.black, size: 12),
                  MyText(
                    title: price,
                    color:  state.data == select ?  MyColors.offWhite: color,
                    size: 20,
                  ),
                  MyText(
                      title: "ر.س", color: color ?? MyColors.primary, size: 12),
                  MyText(
                      title: "شهريا", color: color ?? MyColors.black, size: 12),
                ],
              ),
            ),
          );
        });
  }
}
