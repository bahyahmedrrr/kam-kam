part of 'SubscribtionPackageImports.dart';
class SubscribtionPackage extends StatefulWidget {
  const SubscribtionPackage({Key? key}) : super(key: key);

  @override
  _SubscribtionPackageState createState() => _SubscribtionPackageState();
}

class _SubscribtionPackageState extends State<SubscribtionPackage> {
  final SubscribtionPackageData data = SubscribtionPackageData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: "باقات الاشتراك", hasNotify: false,),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        children: [
          Image.asset(Res.footactive, scale: 2, color: MyColors.black.withOpacity(.7), height: MediaQuery.of(context).size.height*.15,),
          BuildSelectPackage(data: data,),
          BuildFeaturesOfPackage(),
           DefaultButton(title: "توقع", onTap: ()=> AutoRouter.of(context).push(SelectPackageRoute()))
        ],
      ),
    );
  }
}
