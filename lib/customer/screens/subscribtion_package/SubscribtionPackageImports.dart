import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/subscribtion_package/widgets/SubscribtionPackageWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'SubscribtionPackage.dart';
part 'SubscribtionPackageData.dart';