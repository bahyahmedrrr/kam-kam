part of 'MatchResultWidgetsImports.dart';

class BuildResultView extends StatelessWidget {
  const BuildResultView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: MyColors.primary,

              ),
              padding: const EdgeInsets.all(5),
              child: MyText(
                alien: TextAlign.center,
                title: "3",
                size: 12,
                color: MyColors.offWhite,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: MyText(
              alien: TextAlign.center,
              title: "vs",
              color: MyColors.black,
              size: 12,
              fontWeight: FontWeight.bold,

            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: MyColors.offWhite,
              ),
              padding: const EdgeInsets.all(5),
              child: MyText(
                alien: TextAlign.center,
                fontWeight: FontWeight.bold,
                title: "3",
                color: MyColors.primary,
                size: 12,
              ),
            ),
          )
        ],
      ),
    );
  }
}
