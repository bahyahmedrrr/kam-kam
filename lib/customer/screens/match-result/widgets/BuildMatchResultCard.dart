part of 'MatchResultWidgetsImports.dart';

class BuildMatchResultCard extends StatelessWidget {
  const BuildMatchResultCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.offWhite,
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CachedImage(
                borderRadius: BorderRadius.circular(10),
                  height: 100,
                  width: 100,
                  url:
                      "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                child: Column(
                  children: [
                    Image.asset(
                      Res.stadium,
                      scale: 3,
                    ),
                    MyText(
                        title: "الجوهره المشعه",
                        color: MyColors.black,
                        size: 10),
                  ],
                ),
              ),
              CachedImage(
                  borderRadius: BorderRadius.circular(10),
                  height: 100,
                  width: 100,
                  url:
                      "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
            ],
          )
        ],
      ),
    );
  }
}
