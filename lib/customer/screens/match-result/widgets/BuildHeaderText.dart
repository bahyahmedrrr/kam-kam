part of 'MatchResultWidgetsImports.dart';
class BuildHeaderText extends StatelessWidget {
  const BuildHeaderText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: MyText(
        alien: TextAlign.start,
          title: "نتيجة المباراة",
          color: MyColors.primary,
          size: 14,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
