part of 'MatchResultImports.dart';
class MatchResult extends StatefulWidget {
  const MatchResult({Key? key}) : super(key: key);

  @override
  _MatchResultState createState() => _MatchResultState();
}

class _MatchResultState extends State<MatchResult> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "صفحة المباراة", hasNotify: false,
      ),
      body: ListView(
        children: [
          BuildHeaderText(),
          BuildMatchResultCard(),
          BuildResultView()
        ],
      ),
    );
  }
}
