import 'package:base_flutter/customer/screens/match-result/widgets/MatchResultWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';

part 'MatchResult.dart';
part 'MatchResultData.dart';