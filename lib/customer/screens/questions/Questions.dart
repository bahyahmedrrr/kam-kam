part of 'QuestionsImports.dart';
class Questions extends StatefulWidget {
  const Questions({Key? key}) : super(key: key);

  @override
  _QuestionsState createState() => _QuestionsState();
}

class _QuestionsState extends State<Questions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "الاسئله المتكرره", hasNotify: false,),
        body: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            itemBuilder: (context, index) => BuildQuestionField()));
  }
}
