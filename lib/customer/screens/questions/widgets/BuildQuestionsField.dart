part of 'QuestionsWidgetsImports.dart';
class BuildQuestionField extends StatelessWidget {
  const BuildQuestionField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 11),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: MyColors.white,
              border:
              Border.all(color: MyColors.grey.withOpacity(.4), width: .5),
              boxShadow: [BoxShadow(color: MyColors.black.withOpacity(.4))]),
          child: Row(
            children: [
              MyText(
                title: 'س : ',
                color: MyColors.black,
                size: 9,
                fontWeight: FontWeight.bold,
              ),
              Expanded(
                child: MyText(
                  title: 'هذا النص هو مثال السؤال؟',
                  color: MyColors.black,
                  size: 10,
                  fontWeight: FontWeight.bold,
                ),
              ),
              InkWell(
                onTap: () {},
                child: Icon(
                  Icons.keyboard_arrow_down,
                  color: MyColors.black,
                  size: 15,
                ),
              )
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
          margin: const EdgeInsets.symmetric(horizontal: 15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: MyColors.black.withOpacity(.03)),
          child: MyText(
            title:
            ' هذا النص هو مثال السؤال هذا النص هو مثال السؤال هذا النص هو مثال السؤال هذا النص هو مثال السؤال هذا النص هو مثال السؤال هذا النص هو مثال السؤال',
            color: MyColors.primary,
            size: 9.5,
          ),
        ),
      ],
    );

  }
}
