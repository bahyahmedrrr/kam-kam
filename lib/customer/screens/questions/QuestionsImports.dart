import 'package:base_flutter/customer/screens/questions/widgets/QuestionsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';

part 'QuestionsData.dart';
part 'Questions.dart';
