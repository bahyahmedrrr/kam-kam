part of 'ProfileImports.dart';
class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final ProfileData profileData = ProfileData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "تعديل بياناتي", hasNotify: false,),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          children: [
            BuildProfileInputs(profileData: profileData,),
          ],
        ),
        bottomSheet: DefaultButton(onTap: () {}, title: 'حفظ',),
      );
  }
}
