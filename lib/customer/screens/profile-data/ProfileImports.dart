import 'package:base_flutter/customer/screens/profile-data/widgets/ProfileWidgetsImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'Profile.dart';
part 'ProfileData.dart';