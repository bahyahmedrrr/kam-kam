part of 'ProfileImports.dart';


class ProfileData{
  final TextEditingController name = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController country = TextEditingController();
}