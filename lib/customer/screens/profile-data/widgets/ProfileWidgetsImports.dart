import 'package:base_flutter/customer/screens/profile-data/ProfileImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_validator/tf_validator.dart';

import '../../complaints/widgets/ComplaintsWidgetsImports.dart';

part 'BuildProfileInputs.dart';
