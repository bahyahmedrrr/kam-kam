part of 'ProfileWidgetsImports.dart';
class BuildProfileInputs extends StatelessWidget {
  final ProfileData profileData ;
  const BuildProfileInputs({required this.profileData});

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InputName(
            text: 'اسم المستخدم',
          ),
          GenericTextField(
            hint: "نص افتراضي",
            fieldTypes: FieldTypes.normal,
            margin: const EdgeInsets.symmetric(vertical: 5),
            contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            controller: profileData.name,
            fillColor: MyColors.offWhite,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
          ),
          InputName(
            text: 'رقم الجوال',
          ),
          GenericTextField(
            hint: "نص افتراضي",
            fieldTypes: FieldTypes.normal,
            margin: const EdgeInsets.symmetric(vertical: 5),
            contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            controller: profileData.phone,
            fillColor: MyColors.offWhite,
            type: TextInputType.number,
            action: TextInputAction.next,
            validate: (value) => value!.validatePhone(context),
          ),
          InputName(
            text: 'البريد الاكتروني',
          ),
          GenericTextField(
            hint: "نص افتراضي",
            fieldTypes: FieldTypes.normal,
            margin: const EdgeInsets.symmetric(vertical: 5),
            contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            controller: profileData.name,
            fillColor: MyColors.offWhite,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmail(context),
          ),
          InputName(
            text: 'الدوله',
          ),
          GenericTextField(
            hint: "نص افتراضي",
            fieldTypes: FieldTypes.normal,
            margin: const EdgeInsets.symmetric(vertical: 5),
            contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            controller: profileData.country,
            fillColor: MyColors.offWhite,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
          ),
          InputName(
            text: 'اسم المستخدم',
          ),
          GenericTextField(
            hint: "نص افتراضي",
            fieldTypes: FieldTypes.normal,
            margin: const EdgeInsets.symmetric(vertical: 5),
            contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            controller: profileData.name,
            fillColor: MyColors.offWhite,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value!.validateEmpty(context),
          ),
        ],
      ),
    );
  }
}
