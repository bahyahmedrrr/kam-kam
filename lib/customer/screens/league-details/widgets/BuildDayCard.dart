part of 'LeagueDetailsWidgetsImports.dart';

class BuildDayCard extends StatelessWidget {
  const BuildDayCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          children: List.generate(
              2,
              (index) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(title: "الاتنين 7 مارس", color: MyColors.primary, size: 12),
                      BuildResultCard(
                          result: "4",
                          result2: "2",
                          name: "اتجاه جده",
                          name1: "الهلال",
                          isEnd: true),
                      BuildResultCard(
                          result: "4",
                          result2: "2",
                          name: "اتجاه جده",
                          name1: "الهلال",
                          isEnd: true),
                      BuildResultCard(
                          result: "4",
                          result2: "2",
                          name: "اتجاه جده",
                          name1: "الهلال",
                          isEnd: false),
                      BuildResultCard(
                          result: "4",
                          result2: "2",
                          name: "اتجاه جده",
                          name1: "الهلال",
                          isEnd: false),
                    ],
                  ))),
    );
  }
}
