part of 'LeagueDetailsImports.dart';
class LeagueDetails extends StatefulWidget {
  const LeagueDetails({Key? key}) : super(key: key);

  @override
  _LeagueDetailsState createState() => _LeagueDetailsState();
}

class _LeagueDetailsState extends State<LeagueDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "الدوري السعودي للمحترفين", hasNotify: false,
      ),
      body: ListView(
        children: [
          BuildDayCard()
        ],
      ),
    );
  }
}
