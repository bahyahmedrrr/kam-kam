import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/register/widgets/RegisterWidgetsImports.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/AuthScaffold.dart';
import 'package:base_flutter/general/widgets/HeaderLogo.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/DefaultButton.dart';

import '../../../general/constants/MyColors.dart';



part 'Register.dart';
part 'RegisterData.dart';