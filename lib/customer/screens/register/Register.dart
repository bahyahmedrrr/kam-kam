part of 'RegisterImports.dart';

 class Register extends StatefulWidget {
   const Register({Key? key}) : super(key: key);
   @override
   _ReisterState createState() => _ReisterState();
 }
 class _ReisterState extends State<Register> {
   @override
   Widget build(BuildContext context) {
     return AuthScaffold(
       child: ListView(
         children: [
           BuildInputs(),
           DefaultButton(
             title: "تسجيل",
             onTap: () => AutoRouter.of(context).push(
               SwitchActiveMethodRoute(),
             ),
           ),
           BuildLoginButton()
         ],
       ),
    );
  }
}
