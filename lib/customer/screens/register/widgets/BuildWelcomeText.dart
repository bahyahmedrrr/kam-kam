part of 'RegisterWidgetsImports.dart';
class BuildWelcomeText extends StatelessWidget {
  const BuildWelcomeText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MyText(title: "أهلا بك و بعودتك", color: MyColors.black, size: 13, fontWeight: FontWeight.bold,),
          MyText(title: "سجل حسابك الجديد و انضم الينا", color: MyColors.primary, size: 11),
        ],
      ),
    );
  }
}
