part of 'RegisterWidgetsImports.dart';
class BuildLoginButton extends StatelessWidget {
  const BuildLoginButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title:"هل لديك حساب ؟",
                size: 12,
                alien: TextAlign.center,
                color: MyColors.black,
              ),
              InkWell(
                onTap: ()=> AutoRouter.of(context).push(LoginRoute()),
                child: MyText(
                  title:"تسجيل الدخول",
                  size: 12,
                  alien: TextAlign.center,
                  color: MyColors.primary,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
