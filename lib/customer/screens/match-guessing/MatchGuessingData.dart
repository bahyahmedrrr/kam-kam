part of 'MatchGuessingImports.dart';

class MatchGuessingData {

  final GlobalKey<DropdownSearchState> firstTeam = GlobalKey();
  final GlobalKey<DropdownSearchState> secondTeam = GlobalKey();

  DropDownModel? firstTeamModel;
  DropDownModel? secondTeamModel;

  changeFirstTeam(DropDownModel? model) {
    firstTeamModel = model;
  }

  void changeSecondTeam(DropDownModel? model) {
    firstTeamModel = model;
  }

  buildButtomSheet(BuildContext context) {
    return showModalBottomSheet(
            shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusDirectional.only(
            topStart: Radius.circular(25), topEnd: Radius.circular(50))),
            context: context,
            builder: (ctx) => BuildNumbersSheet());
  }
}
