import 'package:base_flutter/customer/models/drop_down_model.dart';
import 'package:base_flutter/customer/screens/match-guessing/widgets/MatchGuessingWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:tf_custom_widgets/Inputs/custom_dropDown/CustomDropDown.dart';

part 'MatchGuessingData.dart';
part 'MatchGuessing.dart';
