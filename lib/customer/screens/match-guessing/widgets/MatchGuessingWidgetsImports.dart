import 'package:base_flutter/customer/models/drop_down_model.dart';
import 'package:base_flutter/customer/screens/match-guessing/MatchGuessingImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildGuessHeaderText.dart';
part 'BuildMatchDetailsCard.dart';
part 'BuildGuessInput.dart';
part 'BuildNumbersSheets.dart';