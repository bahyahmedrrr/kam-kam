part of 'MatchGuessingWidgetsImports.dart';

class BuildGuessInput extends StatelessWidget {
  final MatchGuessingData matchGuessingData;

  const BuildGuessInput({Key? key, required this.matchGuessingData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: DropdownTextField<DropDownModel>(
              label: "توقع",
              dropKey: matchGuessingData.firstTeam,
              fillColor: MyColors.offWhite,
              margin: const EdgeInsets.symmetric(horizontal: 5),
              enableColor: MyColors.blackOpacity,
              validate: (value) =>value.validateEmpty(context),
               onChange: matchGuessingData.changeFirstTeam,
              data: List.generate(
                  10, (index) => DropDownModel(id: index, name: "${index}")),
              selectedItem: matchGuessingData.firstTeamModel,
              useName: true,
              textSize: 12,
              arrowBtnPadding: EdgeInsets.zero,
            ),
          ),
          Expanded(
            flex: 3,
            child: MyText(
              alien: TextAlign.center,
              title: "vs",
              color: MyColors.black,
              size: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          Expanded(
            flex: 3,
            child: DropdownTextField<DropDownModel>(
              label: "توقع",
              dropKey: matchGuessingData.secondTeam,
              fillColor: MyColors.offWhite,
              margin: const EdgeInsets.symmetric(horizontal: 5),
              enableColor: MyColors.blackOpacity,
              validate: (value) =>value.validateEmpty(context),
              onChange: matchGuessingData.changeSecondTeam,
              data: List.generate(
                  10, (index) => DropDownModel(id: index, name: "${index}")),
              selectedItem: matchGuessingData.secondTeamModel,
              useName: true,
              textSize: 12,
              arrowBtnPadding: EdgeInsets.zero,
            ),
          ),
        ],
      ),
    );
  }
}
