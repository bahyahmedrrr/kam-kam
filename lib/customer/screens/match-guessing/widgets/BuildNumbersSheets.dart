part of 'MatchGuessingWidgetsImports.dart';

class BuildNumbersSheet extends StatelessWidget {
  const BuildNumbersSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: List.generate(
          11,
          (index) => Container(
            margin: const EdgeInsets.symmetric(vertical: 2),
            width: double.infinity,
            child: Center(child: Column(
              children: [
                MyText(title: "$index", color: MyColors.black, size: 12, fontWeight: FontWeight.bold,),
                Divider()
              ],
            )),
          ),
        ),
      ),
    );
  }
}
