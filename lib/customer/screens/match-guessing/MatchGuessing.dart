part of 'MatchGuessingImports.dart';
class MatchGuessing extends StatefulWidget {
  const MatchGuessing({Key? key}) : super(key: key);

  @override
  _MatchGuessingState createState() => _MatchGuessingState();
}

class _MatchGuessingState extends State<MatchGuessing> {
  final MatchGuessingData matchGuessingData = MatchGuessingData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "صفحة المباراه", hasNotify: false,),
      body: ListView(
        children: [
          BuildGuessHeaderText(),
          BuildMatchDetailsCard(),
          BuildGuessInput(matchGuessingData: matchGuessingData,)
        ],
      ),
    );
  }
}
