import 'package:base_flutter/customer/screens/alerts/widgets/AlertsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';

part 'AlertsData.dart';
part 'Alerts.dart';
