part of 'AlertsImports.dart';
class Alerts extends StatefulWidget {
  const Alerts({Key? key}) : super(key: key);
  @override
  _AlertsState createState() => _AlertsState();
}
class _AlertsState extends State<Alerts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "الاشعارات",
        hasNotify: true,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        children: [
          BuildActiveAccountNotify(),
          BuildLoseNotify(),
          BuildWinNotify()
        ],
      ),
    );
  }
}
