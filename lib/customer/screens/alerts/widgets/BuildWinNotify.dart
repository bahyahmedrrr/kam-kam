part of 'AlertsWidgetsImports.dart';

class BuildWinNotify extends StatelessWidget {
  const BuildWinNotify({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: MyColors.card1bg,
      ),
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(Res.info, scale: 3,),
          MyText(title: "تهانيا تم تفعيل حسابك بنجاح", color: MyColors.primary, size: 10)
        ],
      ),
    );
  }
}
