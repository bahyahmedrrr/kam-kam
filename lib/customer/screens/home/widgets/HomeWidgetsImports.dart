import 'package:base_flutter/customer/screens/home/HomeImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/my-account/MyAccountImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/championships/ChampionShipsImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/main-page/MainPageImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/more/MoreImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/prediction-order/PredictionOrderImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

part 'BuildTabsScreens.dart';
part 'BuildTabBar.dart';
part 'BuildTabItem.dart';