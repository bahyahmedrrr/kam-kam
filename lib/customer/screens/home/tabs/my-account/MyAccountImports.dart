import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/tabs/my-account//widgets/AccountWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

part 'MyAccount.dart';
part 'MyAccountData.dart';
