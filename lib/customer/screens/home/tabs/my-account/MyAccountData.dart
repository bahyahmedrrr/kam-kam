part of 'MyAccountImports.dart';
class MyAccountData {
  GenericBloc<File?> imageBloc = new GenericBloc(null);

  void getProfileImage(BuildContext context) async {
    var image = await Utils.getImage();
    if (image != null) {
      imageBloc.onUpdateData(image);
    }
  }
}