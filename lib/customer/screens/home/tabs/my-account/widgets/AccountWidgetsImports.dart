import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/tabs/my-account/MyAccountImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

part 'BuildProImage.dart';
part 'BuildButtons.dart';
part 'BuildEncourageCard.dart';