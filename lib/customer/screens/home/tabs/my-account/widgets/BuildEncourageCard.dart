part of 'AccountWidgetsImports.dart';

class BuildEncourageCard extends StatelessWidget {
  const BuildEncourageCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: MyColors.offWhite,
          borderRadius: BorderRadius.circular(15)
      ),
      padding: const EdgeInsets.symmetric(vertical: 30),
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          MyText(
            title: "ايش تشجع ؟",
            color: MyColors.primary,
            size: 14,
            fontWeight: FontWeight.bold,
          ),
          MyText(title: "فريقك المحلي", color: MyColors.primary, size: 14),
          Column(children: List.generate(
            2,
            (index) => InkWell(
              onTap: ()=> AutoRouter.of(context).push(TeamMatchesRoute()),
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                width: MediaQuery.of(context).size.width * .5,
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                decoration: BoxDecoration(
                    color: MyColors.bg, borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      CachedImage(
                          width: 20,
                          height: 20,
                          url:
                              "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
                      MyText(title: "الاهلي", color: MyColors.black, size: 14),
                    ]),
                  ],
                ),
              ),
            ),
          )),
        ],
      ),
    );
  }
}
