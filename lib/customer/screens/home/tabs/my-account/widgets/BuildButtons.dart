part of 'AccountWidgetsImports.dart';

class BuildButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: DefaultButton(
          title: "تعديل بياناتي",
          onTap: () => AutoRouter.of(context).push(ProfileRoute()),
        )),
        Expanded(
            child: DefaultButton(
          title: "تغيير كلمة المرور",
          onTap: () => AutoRouter.of(context).push(ChangePasswordRoute()),
        )),
      ],
    );
  }
}
