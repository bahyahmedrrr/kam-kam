part of 'AccountWidgetsImports.dart';

class BuildProImage extends StatelessWidget {
  final MyAccountData myAccountData;

  const BuildProImage({required this.myAccountData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
          bloc: myAccountData.imageBloc,
          builder: (_, state) {
            if (state.data != null) {
              return Container(
                width: 120,
                height: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  image: DecorationImage(
                      image: FileImage(state.data!), fit: BoxFit.fill),
                ),
              );
            } else {
              return CachedImage(
                borderRadius: BorderRadius.circular(100),
                height: 100,
                width: 100,
                fit: BoxFit.fill,
                url:
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTX7RMtWCzFitQvVxbvE3FL4e_pi6NIfyZYVUDn3Q73hjQ1w100C_kRObzEWmRu1u2UIb4&usqp=CAU',
              );
            }
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(Res.egyp, scale: 3,),
            MyText(
              title: "أحمد مكاوي",
              color: MyColors.black,
              size: 12,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(Res.trophy, scale: 4,),
            MyText(
              title: "خبير(1234)",
              color: MyColors.primary,
              size: 12,
            ),
          ],
        ),

      ],
    );
  }
}
