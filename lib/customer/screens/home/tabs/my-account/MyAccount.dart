part of 'MyAccountImports.dart';

class MyAccount extends StatefulWidget {
  const MyAccount({Key? key}) : super(key: key);

  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  final MyAccountData myAccountData = MyAccountData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        leading: Text(""),
        title: "حسابي",
        hasNotify: true,
      ),
      backgroundColor: MyColors.bg,
      body: ListView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(vertical:20, horizontal: 15),
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            decoration: BoxDecoration(
              color: MyColors.offWhite,
              borderRadius: BorderRadius.circular(15)
            ),
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: [
                BuildProImage(myAccountData: myAccountData,),
                BuildButtons(),
                MyText(
                  title: "حذف الحساب",
                  color: Colors.red,
                  size: 12,
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline,
                )
              ],
            ),
          ),
          BuildEncourageCard()
        ],
      ),
    );
  }
}
