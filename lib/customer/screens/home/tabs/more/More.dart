part of 'MoreImports.dart';

class More extends StatefulWidget {
  const More({Key? key}) : super(key: key);

  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "المزيد",
        hasNotify: true,

      ),
      body: ListView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        children: [
          BuildMoreCard(
              logo: Image.asset(
                Res.lang,
                scale: 3.3,
              ),
              title: "اللغه",
              subTitle: "التحكم في اللغه",
              onTab: () => AutoRouter.of(context).push(LangaugeRoute())),
          BuildMoreCard(
            logo: Image.asset(
              Res.ques,
              scale: 4,
            ),
            title: "أسئله متكرره",
            subTitle: "الإطلاع علي الأسئله المتكرره",
            onTab: () => AutoRouter.of(context).push(QuestionsRoute()),
          ),
          BuildMoreCard(
            logo: Image.asset(
              Res.fav,
              scale: 4,
            ),
            title: "المفضله",
            subTitle: "الإطلاع علي الأنديه المفضله",
            onTab: () => AutoRouter.of(context).push(FavouriteRoute()),
          ),
          BuildMoreCard(
            logo: Image.asset(
              Res.sharrot,
              scale: 4,
            ),
            title: "الشروط و الأحكام",
            subTitle: "الإطلاع علي الأنديه المفضله",
            onTab: () => AutoRouter.of(context).push(TermsRoute()),
          ),
          BuildMoreCard(
            logo: Image.asset(
              Res.about,
              scale: 1.2,
            ),
            title: "حول التطبيق",
            subTitle: "كل ما تريد معرفته عن التطبيق",
            onTab: () => AutoRouter.of(context).push(AboutRoute()),
          ),
          BuildMoreCard(
              logo: Image.asset(
                Res.phone,
                scale: 4,
              ),
              title: "إتصل بنا",
              subTitle: "هل تواجه مشكله ؟ سنساعدك في حلها",
              onTab: () => AutoRouter.of(context).push(ContactUsRoute())),
          BuildMoreCard(
              logo: Image.asset(
                Res.share,
                scale: 4,
              ),
              title: "مشاركة التطبيق",
              subTitle: "شارك التطبيق مع اصدقائك",
              onTab: () {}),
          BuildMoreCard(
              logo: Image.asset(
                Res.shkwa,
                scale: 4,
              ),
              title: "الشكاوي والإقتراحات",
              subTitle: "ارسل شكوتك أو اقتراحك هنا",
              onTab: () => AutoRouter.of(context).push(ComplaintsRoute())),
          BuildMoreCard(
              logo: Image.asset(
                Res.logout,
                scale: 4,
              ),
              title: "تسجيل الخروج",
              subTitle: "يمكنك تسجيل الخروج من حسابك من هنا",
              onTab: () => AutoRouter.of(context).push(LoginRoute())),
        ],
      ),
    );
  }
}
