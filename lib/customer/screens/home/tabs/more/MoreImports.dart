import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/tabs/more/widgets/MoreWidgetsImports.dart';
import 'package:base_flutter/customer/screens/langauage/LangaugeImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

part 'MoreData.dart';
part 'More.dart';
