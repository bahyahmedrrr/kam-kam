part of 'PredictionOrderWidgetsImports.dart';

class BuildOrderBar extends StatelessWidget {
  const BuildOrderBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: List.generate(7, (index) => Container(
          padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
          margin: const EdgeInsets.symmetric(horizontal: 6,  vertical: 6),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: MyColors.offWhite,
          ),
          child: Row(
            children: [
              Image.asset(Res.trophy, scale: 4,),
              MyText(title: "الدوري السعودي", color: MyColors.black, size: 10)
            ],
          ),
        )),
      ),
    );
  }
}
