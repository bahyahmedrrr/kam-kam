part of 'PredictionOrderWidgetsImports.dart';
class BuildNamesRow extends StatelessWidget {
  const BuildNamesRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.primary,
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(title: "الترتيب", color: MyColors.white, size: 12),
              MyText(title: "الاسم", color: MyColors.white, size: 10, fontWeight: FontWeight.bold,),
              MyText(title: "عدد النقاط", color: MyColors.white, size: 10),
            ],
          ),
        ],
      ),
    );
  }
}
