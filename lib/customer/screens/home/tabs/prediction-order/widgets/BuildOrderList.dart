part of 'PredictionOrderWidgetsImports.dart';

class BuildExpectedCard extends StatelessWidget {
  final int index;
  const BuildExpectedCard({required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(title: "$index", color: MyColors.black, size: 12),
              MyText(title: "أحمد مكاوي", color: MyColors.black, size: 10, fontWeight: FontWeight.bold,),
              MyText(title: "12345", color: MyColors.primary, size: 10),
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}
