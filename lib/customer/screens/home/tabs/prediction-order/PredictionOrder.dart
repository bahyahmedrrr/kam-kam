part of 'PredictionOrderImports.dart';

class PredictionOrder extends StatefulWidget {
  const PredictionOrder({Key? key}) : super(key: key);

  @override
  _PredictionOrderState createState() => _PredictionOrderState();
}

class _PredictionOrderState extends State<PredictionOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        leading: Text(""),
        title: "ترتيب المتوقعين",
        hasNotify: true,
      ),
      body: Column(
        children: [
          BuildOrderBar(),
          BuildNamesRow(),
          Flexible(
            child: ListView.builder(
              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                shrinkWrap: true,
                itemCount: 21,
                itemBuilder: (context, index) => BuildExpectedCard(
                      index: index,
                    )),
          ),
        ],
      ),
    );
  }
}
