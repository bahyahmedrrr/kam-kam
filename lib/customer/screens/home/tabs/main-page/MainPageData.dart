part of 'MainPageImports.dart';
class MainPageData{
  final GenericBloc<String> fromCubit = new GenericBloc("");

  setDate(BuildContext context) {
    AdaptivePicker.datePicker(
      context: context,
      title: "تبدأ من",
      onConfirm: (time) {
        if (time != null) {
          fromCubit.onUpdateData(DateFormat("hh:mm a", "ar").format(time));
          AutoRouter.of(context).pop();
        }
      },
    );
  }
}