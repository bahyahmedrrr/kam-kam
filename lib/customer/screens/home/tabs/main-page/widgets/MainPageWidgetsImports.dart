import 'package:base_flutter/customer/screens/home/tabs/main-page/MainPageImports.dart';
import 'package:base_flutter/customer/widgets/BuildResultCard.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildSwiper.dart';
part 'BuildMostImportantMatches.dart';
part 'BuildSearchToolBar.dart';