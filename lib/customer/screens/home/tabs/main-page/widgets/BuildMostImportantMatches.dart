part of 'MainPageWidgetsImports.dart';

class BuildMostImportantMatches extends StatelessWidget {
  const BuildMostImportantMatches({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              color: MyColors.offWhite,
              child: MyText(
                  title: "كأس خادم الحرمين الشريفين",
                  color: MyColors.black,
                  size: 12)),
          BuildResultCard(
            result: "4",
            result2: "0",
            name: "الشباب",
            name1: "الزمالك",
            isEnd: true,
          ),
          BuildResultCard(
            result: "4",
            result2: "0",
            name: "الشباب",
            name1: "الزمالك",
            isEnd: false,
          ),
        ],
      ),
    );
  }
}
