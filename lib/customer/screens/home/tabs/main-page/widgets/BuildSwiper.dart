part of 'MainPageWidgetsImports.dart';

class BuildSwiper extends StatelessWidget {
  const BuildSwiper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      height: 150,
      width: double.infinity,
      child: Swiper(
          autoplay: true,
          duration: 1,
          pagination: SwiperPagination(),
          itemBuilder: (ctx, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: CachedImage(
                  url:
                  'https://logos-download.com/wp-content/uploads/2016/05/NYX_logo_black.png',
                borderRadius: BorderRadius.circular(10),
              ),
            );
          },
          itemCount: 3),
    );
  }
}
