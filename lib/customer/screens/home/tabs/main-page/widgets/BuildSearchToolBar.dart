part of 'MainPageWidgetsImports.dart';

class BuildSearchToolBar extends StatelessWidget {
  final MainPageData mainPageData;

  const BuildSearchToolBar({required this.mainPageData});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: GenericTextField(
              hint: "ابحث عن ناديك المفضل",
              fieldTypes: FieldTypes.normal,
              type: TextInputType.name,
              action: TextInputAction.go,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 5, horizontal: 8),
              suffixIcon: Image.asset(
                Res.search,
                color: MyColors.grey,
                scale: 3,
              ),
              fillColor: MyColors.offWhite,
              validate: (value) => value!.validateEmpty(context)),
        ),
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: () => mainPageData.setDate(context),
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: 50,
              width: 50,
              color: MyColors.offWhite,
              child: Image.asset(
                Res.calendar,
                scale: 4,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
