part of 'MainPageImports.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final MainPageData mainPageData = MainPageData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "الرئيسيه",
        hasNotify: true,
      ),
      body: ListView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        children: [
          BuildSwiper(),
          BuildSearchToolBar(
            mainPageData: mainPageData,
          ),
          Column(
            children: List.generate(
              3,
              (index) => BuildMostImportantMatches(),
            ),
          )
        ],
      ),
    );
  }
}
