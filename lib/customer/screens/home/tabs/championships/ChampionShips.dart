part of 'ChampionShipsImports.dart';

class ChampionShips extends StatefulWidget {
  const ChampionShips({Key? key}) : super(key: key);

  @override
  _ChampionShipState createState() => _ChampionShipState();
}

class _ChampionShipState extends State<ChampionShips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        leading: Text(""),
        title: "البطولات",
        hasNotify: true,
      ),
      body: ListView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        children: [
          BuildChampionCard(
              name: "الدوري السعودي للحترفين",),
          BuildChampionCard(
            name: "الدوري المصري",
          ),
          BuildChampionCard(
            name: "الدوري الاسباني",
          ),
          BuildChampionCard(
            name: "الدوري الانجليزي",
          ),
          BuildChampionCard(
            name: "دوري ايطال اسيا",
          ),
          BuildChampionCard(
            name: "دوري ابطال اوروبا",
          ),
        ],
      ),
    );
  }
}
