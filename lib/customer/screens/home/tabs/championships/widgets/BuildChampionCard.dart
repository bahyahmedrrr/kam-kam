part of 'ChampionShipWidgetsImports.dart';

class BuildChampionCard extends StatelessWidget {
  final String name;

  const BuildChampionCard({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => AutoRouter.of(context).push(LeagueDetailsRoute()),
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.offWhite,
          borderRadius: BorderRadius.circular(15)
        ),
        child: Row(
          children: [
            CachedImage(
                borderColor: MyColors.grey.withOpacity(.2),
                borderRadius: BorderRadius.circular(20),
                width: 25,
                height: 25,
                url:
                    "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
            const SizedBox(
              width: 10,
            ),
            MyText(
              title: name,
              color: MyColors.black,
              size: 10,
              fontWeight: FontWeight.bold,
            ),
            Spacer(),
            Image.asset(
              Res.lefft,
              scale: 4,
            )
          ],
        ),
      ),
    );
  }
}
