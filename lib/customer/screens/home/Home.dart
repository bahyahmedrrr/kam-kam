part of 'HomeImports.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  final HomeData homeData = HomeData();
  @override
  void initState() {
    homeData.controller = new TabController(length: 5, vsync: this);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: homeData.onBackPressed,
      child: DefaultTabController(
        length: 5,
        initialIndex: 0,
        child: Scaffold(
          body: BuildTabsScreens(
            homeData: homeData,
          ),
          bottomNavigationBar: BuildBottomTabBar(
            data: homeData,
          ),
        ),
      ),
    );
  }
}

