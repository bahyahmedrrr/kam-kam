import 'package:base_flutter/customer/screens/contact-us/widgets/ContactUsWidgetsImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

part 'ContactUsData.dart';
part 'ContactUs.dart';
