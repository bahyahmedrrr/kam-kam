part of 'ContactUsImports.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "اتصل بنا",
        hasNotify: false,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 20),
        children: [
          BuildContactMethodCard(
              image: Res.mail,
              title: "التواصل عبر البريد الالكتروني",
              subTitle: "example.info@gmail.com"),
          BuildContactMethodCard(
              image: Res.callus,
              title: "التواصل عبر الهاتف",
              subTitle: "01234789022"),
          BuildContactMethodCard(
              image: Res.whats,
              title: "التواصل عبر الواتساب",
              subTitle: "01234789022"),
          BuildSocialMediaList()
        ],
      ),
    );
  }
}
