part of 'ContactUsWidgetsImports.dart';

class BuildContactMethodCard extends StatelessWidget {
  final String image;
  final String title;
  final String subTitle;

  const BuildContactMethodCard(
      {required this.image, required this.title, required this.subTitle});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(image, scale: 4,),
          const SizedBox(width: 10,),
          Column(
            children: [
              MyText(
                title: title,
                color: MyColors.black,
                size: 12,
                fontWeight: FontWeight.bold,
              ),
              MyText(title: subTitle,
                  color: MyColors.black,
                  size: 10),
            ],
          )
        ],
      ),
    );
  }
}
