part of 'ContactUsWidgetsImports.dart';
class BuildSocialMediaList extends StatelessWidget {
  const BuildSocialMediaList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 10),
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(Res.instagram, scale: 4,),
          Image.asset(Res.facebook,scale: 4,),
          Image.asset(Res.twitter,scale: 4,),
          Image.asset(Res.linkedin,scale: 4,)
        ],
      ),
    );
  }
}
