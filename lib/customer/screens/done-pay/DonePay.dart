part of 'DonePayImports.dart';
class DonePay extends StatefulWidget {
  const DonePay({Key? key}) : super(key: key);

  @override
  _DonePayState createState() => _DonePayState();
}

class _DonePayState extends State<DonePay> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          BuildDone(),
          BuildText()
        ],
      ),
    );
  }
}
