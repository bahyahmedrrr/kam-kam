part of 'DonePayWidgetsImports.dart';

class BuildDone extends StatelessWidget {
  const BuildDone({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 250),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: MyColors.primary,
      ),
      child:Icon(Icons.done, color: MyColors.offWhite, size: 100,),
    );
  }
}
