part of 'DonePayWidgetsImports.dart';

class BuildText extends StatelessWidget {
  const BuildText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(40),
      child: Column(
        children: [
          MyText(title: "تم ارسال طلبك بنجاح", color: MyColors.black, size: 14),
          MyText(
              title: "سيتم اشعارك عند تفعيل حسابك",
              color: MyColors.black,
              size: 14),
          InkWell(
              onTap: ()=> AutoRouter.of(context).push(HomeRoute()),
              child: MyText(
                  title: "العودة للرئيسيه",
                  color: MyColors.primary,
                  size: 14,
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.bold,
              )),
        ],
      ),
    );
  }
}
