import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/SwitchActiveMethod/widgets/MethodWidgetsImports.dart';
import 'package:base_flutter/customer/screens/register/widgets/RegisterWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/HeaderLogo.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'SwitchActiveMethodData.dart';
part 'SwitchActiveMethod.dart';
