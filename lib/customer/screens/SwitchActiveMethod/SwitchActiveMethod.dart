part of 'SwitchActiveMethodImports.dart';
class SwitchActiveMethod extends StatefulWidget {
  const SwitchActiveMethod({Key? key}) : super(key: key);

  @override
  _SwitchActiveMethodState createState() => _SwitchActiveMethodState();
}

class _SwitchActiveMethodState extends State<SwitchActiveMethod> {
  final SwitchActiveMethodData data = SwitchActiveMethodData();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: MyColors.primary,
        body: ListView(
          physics: BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics(),
          ),
          children: [
            HeaderLogo(),
            Container(
              padding:
              const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  color: MyColors.bg,
                  borderRadius: BorderRadiusDirectional.only(
                      topStart: Radius.circular(50),
                      topEnd: Radius.circular(50))),
              child:Column(
                children: [
                  BuildWelcomeText(),
                  BuildMethodList(data: data,),
                ],
              ),
            )
          ],
        ),
        bottomSheet:  DefaultButton(title: "تأكيد", onTap: ()=> AutoRouter.of(context).push(ActiveAccountRoute(userId: "fghjk")))
        ,
      ),
    );
  }
}
