part of 'MethodWidgetsImports.dart';

class BuildMethodList extends StatelessWidget {
  final SwitchActiveMethodData data;

  const BuildMethodList({required this.data});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: data.type,
        builder: (context, state) {
          return Column(
            children: [
              Row(
                children: [
                  Radio<bool>(
                    value: true,
                    groupValue: state.data,
                    onChanged: (value) {data.type.onUpdateData(value!);
                    print("-------------------7777777234567890${state.data}");
                    }
                  ),
                  MyText(title: "رقم الجوال", color: MyColors.black, size: 12)
                ],
              ),
              MyText(
                  title: "سيتم ارسال كود التفعيل الي رقم جوالك",
                  color: MyColors.grey,
                  size: 12),
              Row(
                children: [
                  Radio<bool>(
                      value: false,
                      groupValue: state.data,
                      onChanged: (value){data.type.onUpdateData(value!);
                      print("-------------------7777777234567890${state.data}");
                      }
                  ),
                  MyText(
                      title: "البريد الالكتروني",
                      color: MyColors.black,
                      size: 12)
                ],
              ),
              MyText(
                  title: "سيتم ارسال كود التفعيل الي بريدك الالكتروني",
                  color: MyColors.grey,
                  size: 12),
            ],
          );
        });
  }
}
