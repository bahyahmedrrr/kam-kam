part of 'LangaugeWidgetsImports.dart';

class BuildSelectLangauge extends StatelessWidget {
  final LangaugeData langaugeData;

  const BuildSelectLangauge({required this.langaugeData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: langaugeData.arEnCubit,
      builder: (context, state) {
        return Container(
          child: Column(
            children: [
              MyText(
                title: "تحديد اللغه",
                color: MyColors.black,
                size: 12,
                fontWeight: FontWeight.bold,
              ),
              Row(
                children: [
                  Radio<bool>(
                      value: true,
                      groupValue: state.data,
                      onChanged: (value) =>
                          langaugeData.arEnCubit.onUpdateData(value!)),
                  MyText(title: "English", color: MyColors.black, size: 12),
                ],
              ),
              Row(
                children: [
                  Radio<bool>(
                      value: false,
                      groupValue: state.data,
                      onChanged: (value) =>
                          langaugeData.arEnCubit.onUpdateData(value!)),
                  MyText(title: "العربيه", color: MyColors.black, size: 12),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
