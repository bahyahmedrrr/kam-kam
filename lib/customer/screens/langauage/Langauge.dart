part of 'LangaugeImports.dart';
class Langauge extends StatefulWidget {
  const Langauge({Key? key}) : super(key: key);

  @override
  _LangaugeState createState() => _LangaugeState();
}

class _LangaugeState extends State<Langauge> {
  final LangaugeData langaugeData = LangaugeData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "اللغه", hasNotify: false,
      ),
      body: Column(
        children: [
          BuildSelectLangauge(langaugeData: langaugeData,)
        ],
      ),
    );
  }
}
