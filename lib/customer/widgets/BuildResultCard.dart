import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildResultCard extends StatelessWidget {
  final bool isEnd;

  final String name;
  final String name1;
  final String result;
  final String result2;
  final void Function()? onTab;

  const BuildResultCard(
      {required this.result,
      required this.result2,
      required this.name,
      required this.name1,
      required this.isEnd,
      this.onTab});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: isEnd == true
          ? () => AutoRouter.of(context).push(MatchResultRoute())
          : () => AutoRouter.of(context).push(MatchGuessingRoute()),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 5),
        margin: const EdgeInsets.symmetric(vertical: 2),
        height: MediaQuery.of(context).size.height * .07,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(color: MyColors.offWhite),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(title: name, color: MyColors.black, size: 14),
            CachedImage(
                width: 20,
                height: 20,
                url:
                    "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              margin: const EdgeInsets.symmetric(horizontal: 10),
              color: MyColors.primary,
              child: Row(
                children: [
                  MyText(
                      title: isEnd == true ? result : "",
                      color: MyColors.offWhite,
                      size: 18),
                  Column(
                    children: [
                      isEnd == true
                          ? MyText(
                              title: "   -   ",
                              color: MyColors.offWhite,
                              size: 18)
                          : Column(
                              children: [
                                Icon(
                                  Icons.access_time_filled,
                                  color: MyColors.offWhite,
                                  size: 15,
                                ),
                                MyText(
                                    title: "8:00 pm",
                                    color: MyColors.offWhite,
                                    size: 12),
                              ],
                            ),
                    ],
                  ),
                  MyText(
                      title: isEnd == true ? result2 : "",
                      color: MyColors.offWhite,
                      size: 18),
                ],
              ),
            ),
            CachedImage(
                width: 20,
                height: 20,
                url:
                    "https://almalnews.com/wp-content/uploads/2020/05/5eaff06024dd3.jpeg"),
            MyText(title: "الاهلي", color: MyColors.black, size: 14),
          ],
        ),
      ),
    );
  }
}
